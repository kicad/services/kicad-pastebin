module kicad-redirect-go

require (
	github.com/caarlos0/env/v6 v6.3.0
	github.com/fasthttp/router v1.3.2
	github.com/gomodule/redigo v1.8.2
	github.com/jackwhelpton/fasthttp-routing v1.0.0
	github.com/klauspost/compress v1.10.11 // indirect
	github.com/valyala/fasthttp v1.16.0
)

go 1.15
