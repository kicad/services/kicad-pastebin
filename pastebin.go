package main

import (
	"crypto/rand"
	"errors"
	"fmt"
	"log"
	"math/big"
	"time"

	"github.com/caarlos0/env/v6"
	"github.com/fasthttp/router"
	"github.com/gomodule/redigo/redis"
	"github.com/valyala/fasthttp"
)

type Config struct {
	RedisHost              string `env:"REDIS_HOST" envDefault:"localhost:6379"`
	RedisPassword          string `env:"REDIS_PASSWORD" envDefault:""`
	RedisDb                string `env:"REDIS_DB" envDefault:""`
	PasteMaxExpiration     int    `env:"PASTE_MAX_EXPIRATION" envDefault:"3600"`
	PasteDefaultExpiration int    `env:"PASTE_DEFAULT_EXPIRATION" envDefault:"1800"`
	PasteMaxLength         int    `env:"PASTE_MAX_LENGTH" envDefault:"3000"`
	ListenPort             int    `env:"LISTEN_PORT" envDefault:"8000"`
}

var redisPool *redis.Pool
var config *Config

var ErrNoPaste = errors.New("no matching paste found")

func findPaste(id string) (*string, error) {
	conn := redisPool.Get()
	defer conn.Close()

	key := "paste_" + id

	ok, err := redis.Bool(conn.Do("EXISTS", key))
	if err != nil {
		return nil, err
	}

	if ok != true {
		return nil, ErrNoPaste
	}

	data, err := redis.String(conn.Do("GET", key))
	if err != nil {
		return nil, err
	}

	return &data, nil
}

func generateRandomString(n int) (string, error) {
	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-"
	var ret []byte
	for i := 0; i < n; i++ {
		num, err := rand.Int(rand.Reader, big.NewInt(int64(len(letters))))
		if err != nil {
			return "", err
		}
		ret = append(ret, letters[num.Int64()])
	}

	return string(ret), nil
}

func getPaste(ctx *fasthttp.RequestCtx) {
	id, ok := ctx.UserValue("id").(string)
	if !ok {
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}

	if id == "" {
		ctx.SetStatusCode(fasthttp.StatusNotFound)
	}

	paste, err := findPaste(id)
	if err == ErrNoPaste {
		ctx.SetStatusCode(fasthttp.StatusNotFound)
		return
	} else if err != nil {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetContentType("plain/text")
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.WriteString(*paste)
	return
}

func postPaste(ctx *fasthttp.RequestCtx) {
	paste := string(ctx.PostBody())

	if len(paste) > config.PasteMaxLength {
		ctx.Error("Paste is too large", fasthttp.StatusBadRequest)
		return
	}

	conn := redisPool.Get()
	defer conn.Close()

	id, err := generateRandomString(16)
	if err != nil {
		ctx.Error("Unable to generate paste id", fasthttp.StatusInternalServerError)
		return
	}

	key := "paste_" + id
	_, err = conn.Do("SET", key, paste)

	if err != nil {
		ctx.Error("Unable to save paste", fasthttp.StatusInternalServerError)
		return
	}

	_, err = conn.Do("EXPIRE", key, config.PasteDefaultExpiration)
	if err != nil {
		ctx.Error("Unable to save paste", fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetContentType("plain/text")
	ctx.SetStatusCode(fasthttp.StatusCreated)
	ctx.WriteString(id)
}

func main() {
	cfg := Config{}
	if err := env.Parse(&cfg); err != nil {
		fmt.Printf("%+v\n", err)
	}
	config = &cfg

	redisPool = &redis.Pool{
		MaxIdle:     10,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", config.RedisHost, redis.DialPassword(config.RedisPassword))
		},
	}

	router := router.New()
	router.GET("/v1/paste/{id}", getPaste)
	router.POST("/v1/paste", postPaste)

	listenString := fmt.Sprintf(":%d", config.ListenPort)
	log.Fatal(fasthttp.ListenAndServe(listenString, router.Handler))
}
