FROM golang:1.15-alpine AS builder

RUN apk add --no-cache git

# Set the Current Working Directory inside the container
WORKDIR /usr/local/src/kicad-pastebin

# Populate module cache
COPY go.mod .
COPY go.sum .

RUN go mod download

# Prepare everything else
COPY . .

# Unit tests
RUN CGO_ENABLED=0 go test -v

# Build the Go app
RUN go build -o ./out/kicad-pastebin .



FROM alpine:3.9 
ENV APP_USER=1000
RUN apk add ca-certificates

RUN mkdir /app
WORKDIR /app
COPY --from=builder /usr/local/src/kicad-pastebin/out/kicad-pastebin /app/kicad-pastebin

RUN chown -R $APP_USER:$APP_USER kicad-pastebin

EXPOSE 8000

USER $APP_USER

CMD ["/app/kicad-pastebin"]